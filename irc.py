import asyncio

import re

import select

import socket

import os

import factorio_rcon

VIPS = ["beginbot", "beginbotbot", "artmattdank", "zanuss"]

def missing_env_var(var_name):
    raise ValueError(
        (
            f"Please populate the {var_name} environment variable to run the bot. "
            "See README for more details."
        )
    )


# Get the value for this here: https://twitchapps.com/tmi/
if "TWITCH_OAUTH_TOKEN" not in os.environ:
    missing_env_var("TWITCH_OAUTH_TOKEN")

if "TWITCH_BOT_NAME" not in os.environ:
    missing_env_var("TWITCH_BOT_NAME")

if "TWITCH_CHANNEL" not in os.environ:
    missing_env_var("TWITCH_CHANNEL")


TOKEN           = os.environ["TWITCH_OAUTH_TOKEN"]
BOT_NAME        = os.environ["TWITCH_BOT_NAME"]
CHANNEL         = os.environ["TWITCH_CHANNEL"]
ENCODING        = "utf-8"
COMMAND_TRIGGER = "!"


def _handshake(server):
    print(f"Connecting to #{CHANNEL} as {BOT_NAME}")
    print(server.send(bytes("PASS " + TOKEN + "\r\n", ENCODING)))
    print(server.send(bytes("NICK " + BOT_NAME + "\r\n", ENCODING)))
    print(server.send(bytes("JOIN " + f"#{CHANNEL}" + "\r\n", ENCODING)))


def _connect_to_twitch():
    connection_data = ("irc.chat.twitch.tv", 6667)
    server = socket.socket()
    server.connect(connection_data)
    _handshake(server)
    return server


def pong(server):
    server.sendall(bytes("PONG" + "\r\n", ENCODING))


def send_message(server, msg):
    server.send(bytes("PRIVMSG " + f"#{CHANNEL}" + " :" + msg + "\n", ENCODING))


def _is_command_msg(msg):
    return msg[0] == COMMAND_TRIGGER and msg[1] != COMMAND_TRIGGER


# TODO: refactor this sillyness
def _parse_user_and_msg(irc_response):
    user_info, _, _, *raw_msg = irc_response.split()
    raw_first_word, *raw_rest_of_the_message = raw_msg
    first_word = raw_first_word[1:]
    rest_of_the_message = " ".join(raw_rest_of_the_message)

    print(f"user_info {user_info}")

    user = user_info.split("!")[0][1:]
    msg = f"{first_word} {rest_of_the_message}"
    return user, msg


def process_msg(irc_response):
    if "PING" in irc_response:
        pong(server)

    split_response = irc_response.split()

    if len(split_response) < 4:
        return "", False

    rawUser, msg = _parse_user_and_msg(irc_response)
    user = rawUser.split("!")[0]

    vip_mode = os.environ.get("VIP", False)
    if vip_mode and user not in VIPS:
            print(f"{user} not a VIP in VIP mode")
            return "", False

    if _is_command_msg(msg):
        rcon_command = construct_factorio_command(msg)
        print(f"COMMAND: {rcon_command}")

        return rcon_command, True
    else:
        return f"$banner {msg}", True

    return f"{user}: {msg}", False

def construct_factorio_command(msg):
    splitMsgs = msg.strip().split(" ")

    if splitMsgs[0] == "!player":
        player_init = "/sc local player = game.get_player(1)"
        command = msg.strip()[1:]
        rcon_command = player_init + "; " + command
        return rcon_command

    if msg[0] == COMMAND_TRIGGER:
        return msg.strip()




def run_bot(server):
    client = factorio_rcon.RCONClient("127.0.0.1", 27015, "password")
    chat_buffer = ""

    # This does give a response
    r = client.send_command("/promote beginbot")
    print(r)

    server.setblocking(0)

    timeout_in_seconds = 0.2
    while True:
        ready = select.select([server], [], [], timeout_in_seconds)
        if ready[0]:
            chat_buffer = chat_buffer + server.recv(2048).decode("utf-8")
            send_notifications(server)
            messages = chat_buffer.split("\r\n")
            chat_buffer = messages.pop()

            for message in messages:
                command, is_command = process_msg(message)

                print(f"Sending Command: {command}")
                if is_command and command:
                    client.send_command(command)
        else:
            send_notifications(server)

def send_notifications(server):
    notifications_path = "/home/begin/.factorio/script-output/notifications.txt"
    with open(notifications_path,"r+") as f:
        line = f.read()
        f.seek(0)
        f.truncate()

        if line:
            msg = line.replace('"', '')
            print(msg)
            send_message(server, f"Factorio: {msg}")

if __name__ == "__main__":
    server = _connect_to_twitch()
    # send_message(server, "Starting Factorio IRC -> RCON")
    run_bot(server)
